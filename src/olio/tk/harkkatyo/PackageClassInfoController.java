//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class PackageClassInfoController implements Initializable {
    @FXML
    private Button CloseButton;
    @FXML
    private Label InfoTextLabel;
    
    private String text;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        text = "1. luokan paketti on kaikista nopein pakettiluokka, jonka "
                + "vuoksi sitä ei voi lähettää pidemmälle kuin 150 km päähän. "
                + "1. luokan paketti on myös nopea, koska sen turvallisuudesta "
                + "ei välitetä niin paljon, jolloin kaikki särkyvät esineet "
                + "tulevat menemään rikki matkan aikana. \nPaketin sisälle "
                + "mahtuvat 50cm x 50cm x 30cm kokoiset esineet."
                + "\n\n"
                + "2. luokan paketit ovat turvakuljetuksia, jolloin ne kestävät "
                + "parhaiten kaiken särkyvän tavaran kuljettamisen. Näitä "
                + "paketteja on mahdollista kuljettaa jopa Lapista Helsinkiin, "
                + "sillä matkan aikana käytetään useampaa kuin yhtä "
                + "TIMOTEI-miestä, jolloin turvallinen kuljetus on taattu. "
                + "Paketissa on kuitenkin huomattava, että jos se on liian "
                + "suuri, ei särkyvä esine voi olla heilumatta, joten paketin "
                + "koon on oltava pienempi kuin muilla pakettiluokilla.\nPaketin "
                + "sisälle mahtuvat 30cm x 30cm x 30cm kokoiset esineet."
                + "\n\n"
                + "3. luokan paketti on TIMOTEI-miehen stressinpurkupaketti. "
                + "Tämä tarkoittaa sitä, että TIMOTEI-miehellä ollessa huono "
                + "päivä pakettia paiskotaan seinien kautta automaatista "
                + "toiseen, joten paketin sisällön on oltava myös erityisen "
                + "kestävää materiaalia. Myös esineen suuri koko ja paino ovat "
                + "eduksi, jolloin TIMOTEI-mies ei jaksa heittää pakettia "
                + "seinälle kovin montaa kertaa. Koska paketit päätyvät "
                + "kohteeseensa aina seinien kautta, on tämä hitain "
                + "mahdollinen kuljetusmuoto paketille."
                + "\nPaketin sisälle mahtuvat 80cm x 70cm x 70cm kokoiset esineet."
                + "\n\nLähde: harjoitustyön tehtävänanto";
        
        InfoTextLabel.setText(text);
    }    

    @FXML
    private void CloseAction(ActionEvent event) {
        Stage stage = (Stage) CloseButton.getScene().getWindow();
        stage.close();
    }
    
}
