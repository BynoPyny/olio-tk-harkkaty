//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static olio.tk.harkkatyo.OptionsMenuController.addError;
import static olio.tk.harkkatyo.PackageUpdateController.updatePackageList;
import static olio.tk.harkkatyo.StatsWindowController.updateStats;
import static olio.tk.harkkatyo.StatsWindowController.updateStorage;

public class MainWindowController implements Initializable {

    private Stage stage1; //settings
    private Stage stage2; //stats
    private Stage stage4; //paketinpäivitys

    private String city;

    private boolean packageUpdaterInitialized;

    @FXML
    private WebView karttaView;
    @FXML
    private Button OpenSettings;
    @FXML
    private Button ExitButton;
    @FXML
    private Button OpenStatsButton;
    @FXML
    private Button RemoveRoutesButton;
    @FXML
    private static ChoiceBox<String> AutomatCityChoiceBox;
    @FXML
    private Button DrawAutomatsButton;
    @FXML
    private Button SendPackageButton;
    @FXML
    private static ChoiceBox<String> SendPackageChoiceBox;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String html = getClass().getResource("index.html").toExternalForm();
        karttaView.getEngine().load(html); //lataa kartta
        packageUpdaterInitialized = false;

        try { //alusta asetukset-ikkuna
            stage1 = new Stage();
            Parent options = FXMLLoader.load(getClass().getResource("OptionsMenu.fxml"));
            Scene scene1 = new Scene(options);
            stage1.setScene(scene1);
            stage1.setTitle("Asetukset");
            stage1.initStyle(StageStyle.UTILITY);
            stage1.setResizable(false);

        } catch (IOException ex) {
            System.out.println("Asetusvalikon alustamisessa tapahtui virhe.");
        }

        try { //alusta statistiikkaikkuna
            stage2 = new Stage();
            Parent statswindow = FXMLLoader.load(getClass().getResource("StatsWindow.fxml"));

            Scene scene2 = new Scene(statswindow);
            stage2.setScene(scene2);
            stage2.setTitle("Tilastot");
            stage2.initStyle(StageStyle.UTILITY);
            stage2.setResizable(false);

        } catch (IOException ex) {
            addError("Statistiikkaikkunan avaamisessa tapahtui virhe.");
        }

    }

    @FXML // avaa asetusikkuna
    private void OpenSettingsAction(ActionEvent event) {

        if (!stage1.isShowing()) { //näytä ikkuna jos suljettu
            stage1.show();
        } else {
            stage1.toFront();
        }
    }

    @FXML
    private void ExitButtonAction(ActionEvent event) {
        System.out.println("Suljetaan ohjelma.");
        System.exit(0);
    }

    @FXML // avaa statistiikkaikkuna
    private void OpenStatsAction(ActionEvent event) {
        //näytä ikkuna jos suljettu
        if (!stage2.isShowing()) {
            stage2.show();
            try {
                updateStats();
                updateStorage();
            } catch (ClassNotFoundException | SQLException ex) {
                addError("Tilastojen päivitys epäonnistui");
            }
        } else {
            stage2.toFront();
        }
    }

    @FXML //poista piirretyt reitit
    private void RemoveRoutesAction(ActionEvent event) {
        karttaView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML //piirrä kaupungin automaatit
    private void DrawAutomatsAction(ActionEvent event) {
        String cityName = AutomatCityChoiceBox.getValue();
        String address, info, script;

        for (SmartPost a : Main.posts) {
            if (a.getCity().toUpperCase().equals(cityName)) {

                address = a.getAddress() + ", " + a.getPostnumber() + ", " + a.getCity();
                info = a.getName() + ", " + a.getHours();
                script = "document.goToLocation('" + address + "', '" + info + "', 'red')";

                karttaView.getEngine().executeScript(script);
            }
        }
    }

    //täytetään kaupunkien lista merkkien piirtämistä varten
    public static void populateCityChoices() {

        if (!Main.posts.isEmpty()) {
            AutomatCityChoiceBox.getItems().clear();
            for (SmartPost a : Main.posts) {

                //jos lista tyhjä, lisää ensimmäinen vastaantuleva
                if (AutomatCityChoiceBox.getItems().isEmpty()) {
                    AutomatCityChoiceBox.getItems().add(a.getCity());

                    //muuten tarkista, löytyykö nimeä jo listalta
                } else if (!AutomatCityChoiceBox.getItems().contains(a.getCity())) {
                    AutomatCityChoiceBox.getItems().add(a.getCity());
                }
            }
        }
    }

    @FXML //lähetä paketti kohteeseensa, jos ei kohdetta, avaa "päivitä paketti"
    private void SendPackageAction(ActionEvent event) {
        ArrayList coords = new ArrayList();
        float slat, slon, dlat, dlon; //koordinaatit lähtöpaikalle ja kohteelle
        int packageclass = 0;
        String script, infos[];
        Package temp = null; //lähetettävä paketti

        if (!SendPackageChoiceBox.getItems().isEmpty()) {

            infos = SendPackageChoiceBox.getValue().split(": ");
            String srcCity = infos[0];
            String packageItem = infos[1];

            for (Package a : Main.packages) {
                if (a.getSource().getCity().toUpperCase().equals(srcCity.toUpperCase())
                        && a.getItem().getName().equals(packageItem)) {

                    if (a.getDest() != null) {
                        temp = a;
                        
                        //arraylistiin koordinaatit
                        slat = temp.getSource().getLocation().getLat();
                        coords.add(slat);

                        slon = temp.getSource().getLocation().getLon();
                        coords.add(slon);

                        dlat = temp.getDest().getLocation().getLat();
                        coords.add(dlat);

                        dlon = temp.getDest().getLocation().getLon();
                        coords.add(dlon);

                        //tarkista paketin luokitus
                        if (temp instanceof FirstClass) {
                            packageclass = 1;
                        } else if (temp instanceof SecondClass) {
                            packageclass = 2;
                        } else if (temp instanceof ThirdClass) {
                            packageclass = 3;
                        }

                        //piirrä reitti, palautusarvo (matka) talteen
                        script = "document.createPath(" + coords + ",'blue'," + packageclass + ")";
                        double dist = (double) karttaView.getEngine().executeScript(script);

                        //tietokantatietojen päivitys
                        try {
                            //tilastotietojen päivitys
                            System.out.println(temp+" "+dist);
                            Main.sql.statsUpdate(temp, dist);
                        } catch (ClassNotFoundException | SQLException ex) {
                            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        try {
                            //varastotaulun päivitys
                            Main.sql.movePackageStorage(temp);
                        } catch (ClassNotFoundException | SQLException ex) {
                            addError("Tietokannan varastotilanteen päivitys epäonnistui");
                        }

                        //ohjelman tilatietojen päivitys
                        temp.setSource(a.getDest());
                        temp.setDest(null);
                        populatePackageChoices();

                    } else {
                        addError("Paketilta puuttuu kohde, avataan paketinpäivitys");
                        //paketinpäivitys
                        if (!packageUpdaterInitialized) {
                            try {
                                stage4 = new Stage();
                                Parent updater = FXMLLoader.load(getClass().getResource("PackageUpdate.fxml"));
                                Scene scene4 = new Scene(updater);
                                stage4.setScene(scene4);
                                stage4.setTitle("Paketin päivitys");
                                stage4.initStyle(StageStyle.UTILITY);
                                stage4.setResizable(false);

                                updatePackageList();

                            } catch (IOException ex) {
                                addError("Paketinpäivityksen ikkunan avaaminen epäonnistui");
                            }
                        }
                        if (!stage4.isShowing()) {
                            updatePackageList();
                            stage4.show();
                        }

                    }
                    break;
                }
            }
            addError("Paketin lähetys onnistui");
        }
    }

    public static void populatePackageChoices() {
        SendPackageChoiceBox.getItems().clear();

        for (Package a : Main.packages) {
            String teksti = a.getSource().getCity() + ": " + a.getItem().getName();
            SendPackageChoiceBox.getItems().add(teksti);
        }
    }
}
