//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

public class SmartPost {

    private final String postnumber;
    private final String city;
    private final String address;
    private final String name; //postitoimipaikka
    private final String openhours;
    private final GeoLoc location;

    public SmartPost(String a, String b, String c, String d, String e, GeoLoc f) {
        postnumber = a;
        city = b;
        address = c;
        name = d;
        openhours = e;
        location = f;
    }

    public String getPostnumber() {
        return postnumber;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getHours() {
        return openhours;
    }

    public GeoLoc getLocation() {
        return location;
    }
}

class GeoLoc {

    private final float lat;
    private final float lon;

    public GeoLoc(float a, float b) {
        lat = a;
        lon = b;
    }

    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }
}
