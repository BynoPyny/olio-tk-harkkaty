//Matias Salohonka
//0438604
//6.7.2016

package olio.tk.harkkatyo;

public class Item {
    private final int height;
    private final int depth;
    private final int width;
    private final String name;
    private final boolean breakable;
    
    public Item(int a, int b, int c, String d, boolean e) {
        height = a;
        depth = b;
        width = c;
        name = d;
        breakable = e;
    }
    
    public int getHeight() {
        return height;
    }
    
    public int getDepth() {
        return depth;
    }
    
    public int getWidth() {
        return width;
    }
    
    public String getName() {
        return name;
    }
    
    public boolean getBreakable() {
        return breakable;
    }
}