//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static olio.tk.harkkatyo.MainWindowController.populatePackageChoices;
import static olio.tk.harkkatyo.OptionsMenuController.addError;

public class PackageCreationController implements Initializable {

    private static boolean packageInfoInitialized;
    private Stage stage4;

    @FXML
    private ChoiceBox<String> ItemMenu;
    @FXML
    private TextField NameField;
    @FXML
    private TextField HeightField;
    @FXML
    private TextField WidthField;
    @FXML
    private TextField DepthField;
    @FXML
    private CheckBox BreakableCheck;
    @FXML
    private RadioButton firstClassRadio;
    @FXML
    private RadioButton secondClassRadio;
    @FXML
    private RadioButton thirdClassRadio;
    @FXML
    private Button GetClassInfo;
    @FXML
    private ChoiceBox<String> SendCityField;
    @FXML
    private ChoiceBox<String> SendAutomatField;
    @FXML
    private ChoiceBox<String> DestCityField;
    @FXML
    private ChoiceBox<String> DestAutomatField;
    @FXML
    private Button CancelButton;
    @FXML
    private Button AcceptButton;
    @FXML
    private ToggleGroup group1;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        packageInfoInitialized = false;

        for (Item a : Main.items) { //täytä esinevalikko
            String text = a.getName() + ", " + a.getWidth() + "x" + a.getHeight() + "x"
                    + a.getDepth() + "cm";
            if(a.getBreakable()) {
                text = text.concat(", särkyvä");
            }
            ItemMenu.getItems().add(text);
        }

        populateCityBox(SendCityField);
        populateCityBox(DestCityField);
    }

    @FXML //"infoa pakettiluokista"
    private void GetClassInfoAction(ActionEvent event) {
        if (!packageInfoInitialized) {
            try {

                stage4 = new Stage();
                Parent stats = FXMLLoader.load(getClass().getResource("PackageClassInfo.fxml"));

                Scene scene4 = new Scene(stats);
                stage4.setScene(scene4);
                stage4.setTitle("Pakettiluokkien info");
                stage4.initStyle(StageStyle.UTILITY);
                stage4.setResizable(false);

                packageInfoInitialized = true;

            } catch (IOException ex) {
                System.out.println("nope");
                addError("Luokitusinfoikkunan avaamisessa tapahtui virhe");
            }
        }

        if (!stage4.isShowing()) {
            stage4.show();
        }
    }

    @FXML
    private void CancelButtonAction(ActionEvent event) {
        Stage stage = (Stage) CancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void AcceptButtonAction(ActionEvent event) {
        int height = 30, width = 30, depth = 30, packageclass = 0, dist;
        ArrayList<String> info = new ArrayList();
        boolean breakable;
        Item a = null; //lopulta paketoitava esine
        Package b = null; //lopullinen paketti

        //luodaan uusi esine
        if (!NameField.getText().isEmpty()) { //tarkistetaan parametrit kentistä

            if (!HeightField.getText().isEmpty()) { //jos kenttä on tyhjä, käytetään oletusarvoa 30
                height = Integer.parseInt(HeightField.getText());
            }
            if (!WidthField.getText().isEmpty()) {
                width = Integer.parseInt(WidthField.getText());
            }
            if (!DepthField.getText().isEmpty()) {
                depth = Integer.parseInt(DepthField.getText());
            }
            if(depth < 1 || width < 1 || height < 1) {
                addError("Esineen mitat eivät voi olla pienempiä kuin 1");
                return;
            }
            
            breakable = BreakableCheck.isSelected();
            info.add(NameField.getText());

            a = new Item(height, depth, width, info.get(0), breakable);
            Main.items.add(a);

            try {
                Main.sql.insertItem(a);
            } catch (ClassNotFoundException | SQLException ex) {
                addError("Esineen lisääminen tietokantaan epäonnistui");
                return;
            }
        } else { //TAI valitaan esine valikosta

            if (!ItemMenu.getSelectionModel().isEmpty()) { //onko valikosta valittu jotain

                info.add(ItemMenu.getValue().split(", ")[0]); //parsitaan valikon tekstistä esineen nimi
                for (Item c : Main.items) { //etsitään esinelista läpi
                    if (c.getName().equals(info.get(0))) {
                        a = c; //esine löydetty
                        break;
                    }
                }
            } else {
                addError("Ei valittu esinettä tai luotu omaa");
                return;
            }
        }
        //tarkistetaan valittu luokka paketille
        if (firstClassRadio.isSelected()) {
            packageclass = 1;
        } else if (secondClassRadio.isSelected()) {
            packageclass = 2;
        } else if (thirdClassRadio.isSelected()) {
            packageclass = 3;
        } else {
            addError("Pakettiluokkaa ei valittu");
            return;
        }

        try {
            //tarkistetaan sopiiko esine pakettiin
            if (Main.sql.checkPackageFit(packageclass, a)) {

                b = Main.sql.createPackage(packageclass); //luodaan tietokannan tiedoista paketti
                b.setItem(a); //esine pakettiin
                try {
                    Main.sql.insertPackage(b, packageclass); //paketti tietokantaan
                } catch(SQLException ex) {
                    addError("Paketin lisääminen tietokantaan epäonnistui");
                }

                b.setSource(findAutomat(SendAutomatField)); //asetetaan ohjelmaa varten lähtö-
                b.setDest(findAutomat(DestAutomatField)); //ja kohdekaupunki

                Main.packages.add(b); //ja paketti esineineen listalle ohjelmaan
                populatePackageChoices();
                
            } else {
                addError("Esine ei sovi pakettiin (liian suuri/särkyvä)");
                return;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            addError("Paketin kokovaatimuksien tarkistus tai paketin luonti epäonnistui");
            return;
        }

        //lisää paketti tietokannan varastoon
        SmartPost kohde = findAutomat(SendAutomatField);
        try {
            Main.sql.addPackageToStorage(b, kohde);
        } catch (ClassNotFoundException | SQLException ex) {
            addError("Paketin lisääminen tietokannan varastoon epäonnistui");
        }

        Stage stage = (Stage) AcceptButton.getScene().getWindow();
        stage.close();
    }

    @FXML //päivittää automaattivalikon avattaessa kaupungin perusteella
    private void SendAutomatFieldAction(ActionEvent event) {
        SendAutomatField.getItems().clear();
        String city = SendCityField.getValue();
        for (SmartPost a : Main.posts) {
            if (a.getCity().equals(city)) {
                SendAutomatField.getItems().add(a.getName());
            }
        }
    }

    @FXML //päivittää automaattivalikon avattaessa kaupungin perusteella
    private void DestAutomatFieldAction(ActionEvent event) {
        DestAutomatField.getItems().clear();
        String city = DestCityField.getValue();
        for (SmartPost a : Main.posts) {
            if (a.getCity().equals(city)) {
                DestAutomatField.getItems().add(a.getName());
            }
        }
    }

    //etsii automaattivalikon valintaa vastaavan SmartPost-olion
    private SmartPost findAutomat(ChoiceBox<String> a) {
        for (SmartPost b : Main.posts) {
            if (a.getValue().equals(b.getName())) {
                return b;
            }
        }
        return null;
    }

    private void populateCityBox(ChoiceBox<String> c) {
        for (SmartPost a : Main.posts) {

            //jos lista tyhjä, lisää ensimmäinen vastaantuleva
            if (c.getItems().isEmpty()) {
                c.getItems().add(a.getCity());

                //muuten tarkista, löytyykö nimeä jo listalta
            } else if (!c.getItems().contains(a.getCity())) {
                c.getItems().add(a.getCity());
            }
        }
    }

}
