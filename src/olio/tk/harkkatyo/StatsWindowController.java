//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import static olio.tk.harkkatyo.OptionsMenuController.addError;
import olio.tk.harkkatyo.SQL.Tilastorivi;

public class StatsWindowController implements Initializable {
    
    private static ArrayList<Tilastorivi> stats;

    @FXML
    private static ListView<String> StatsList;
    @FXML
    private Button CloseButton;
    @FXML
    private static ListView<String> StorageList;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        stats = new ArrayList(); //alusta lista vain kerran

        try {
            updateStats();
        } catch (ClassNotFoundException | SQLException ex) {
            addError("Tilastotietojen haku tietokannasta epäonnistui");
        }
    }

    //päivittää tilasto-välilehden
    public static void updateStats() throws ClassNotFoundException, SQLException {
        String line;

        stats.clear(); //tyhjennä palautuslista
        StatsList.getItems().clear(); //tyhjennä ikkunan lista

        stats = Main.sql.statsCheck(); //nouda tilastotieto, SQL.java
        for (Tilastorivi a : stats) {
            line = "Ajankohta: " + a.getTime() + ", " + a.getSource()
                    + " -> " + a.getDest() + ", Matka: " + a.getDist()
                    + ", Esine: " + a.getName();
            StatsList.getItems().add(line);
        }
    }

    //päivittää varasto-välilehden
    public static void updateStorage() {
        String line;
        int i = 0;

        StorageList.getItems().clear();

        for (Package a : Main.packages) {
            line = a.getSource().getName()+", "+a.getSource().getCity() + ": " + a.getItem().getName();
            StorageList.getItems().add(line);
            i++;
        }
        line = "Paketteja varastossa yhteensä: "+i;
        StorageList.getItems().add(line);
    }

    @FXML
    private void CloseAction(ActionEvent event) {
        Stage stage = (Stage) CloseButton.getScene().getWindow();
        stage.close();
    }

}
