//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

public abstract class Package {

    protected int dist; //etäisyysrajoitus
    protected boolean breaks; //hajoava
    protected int height;
    protected int depth;
    protected int width;
    protected Item item; //esine
    protected SmartPost source; //nykyinen sijainti
    protected SmartPost dest; //kohde

    //SmartPost attribuuttien määritys omilla metodeilla
    protected Package(int b, boolean c, int d, int e, int f) {
        dist = b;
        breaks = c;
        height = d;
        depth = e;
        width = f;
        source = null;
        dest = null;
    }

    protected int getDist() {
        return dist;
    }

    protected int getHeight() {
        return height;
    }

    protected int getDepth() {
        return depth;
    }

    protected int getWidth() {
        return width;
    }

    protected boolean getBreaks() {
        return breaks;
    }

    protected Item getItem() {
        return item;
    }

    protected void setItem(Item a) {
        item = a;
    }
    
    protected SmartPost getSource() {
        return source;
    }
    
    protected SmartPost getDest() {
        return dest;
    }
    
    protected void setSource(SmartPost a) {
        source = a;
    }
    
    protected void setDest(SmartPost a) {
        dest = a;
    }
}

class FirstClass extends Package {

    public FirstClass(int a, boolean b, int c, int d, int e) {
        super(a, b, c, d, e);
    }
}

class SecondClass extends Package {

    public SecondClass(int a, boolean b, int c, int d, int e) {
        super(a, b, c, d, e);
    }
}

class ThirdClass extends Package {

    public ThirdClass(int a, boolean b, int c, int d, int e) {
        super(a, b, c, d, e);
    }
}
