//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static olio.tk.harkkatyo.MainWindowController.populateCityChoices;

public class OptionsMenuController implements Initializable {

    private DataBuilder databuilder;
    private BufferedReader br;
    private Stage stage2 = null;
    private Stage stage3 = null;
    
    private boolean PacketCreatorInitialized;
    private boolean StorageInitialized;

    @FXML
    private Button CreatePacket;
    @FXML
    private Button LoadIntoDatabase;
    @FXML
    private static ListView<String> errorLog;
    @FXML
    private Button CloseButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        PacketCreatorInitialized = false;
        StorageInitialized = false;
    }

    @FXML //poimii automaattien tiedot netistä ja vie tietokantaan & päivittää ohjelman
    private void LoadIntoDatabaseAction(ActionEvent event) {
        try {
            
            String content = getContent();

            databuilder = new DataBuilder(content);

            databuilder.parsePostData();
            addError("Tieto viety tietokantaan");
            
            Main.sql.loadPosts();
            populateCityChoices();
            addError("Ohjelman tiedot päivitetty tietokannasta");

        } catch (MalformedURLException ex) {
            addError("Tiedon hakeminen epäonnistui");
            
        } catch (SQLException | ClassNotFoundException ex) {
            addError("Tietojen siirtäminen tietokantaan epäonnistui");
            
        } catch (IOException ex) {
            addError("Tiedon hakeminen epäonnistui");
        }
    }

    // hakee URL:n lähdekoodin (== XML-raakadata)
    private String getContent() throws MalformedURLException, IOException {
        String content = "", line;
        URL url = new URL("http://smartpost.ee/fi_apt.xml");

        br = new BufferedReader(new InputStreamReader(url.openStream()));

        while ((line = br.readLine()) != null) {
            content += line + "\n";
        }

        return content;
    }

    @FXML //avaa paketinluoja ikkuna
    private void CreatePacketAction(ActionEvent event) {
        if(!PacketCreatorInitialized) {
            try {
                stage2 = new Stage();
                Parent packet = FXMLLoader.load(getClass().getResource("PackageCreation.fxml"));
                Scene scene2 = new Scene(packet);
                stage2.setScene(scene2);
                stage2.setTitle("Luo paketti");
                stage2.initStyle(StageStyle.UTILITY);
                stage2.setResizable(false);
                
                PacketCreatorInitialized = true;

            } catch (IOException ex) {
                addError("Paketinluonnin ikkunan avaaminen epäonnistui");
            }
        }
        if (!stage2.isShowing()) {
            stage2.show();
        } else {
            stage2.toFront();
        }
    }
    
    public static void addError(String a) {
        errorLog.getItems().add(a);
    }

    @FXML
    private void CloseAction(ActionEvent event) {
        Stage stage = (Stage) CloseButton.getScene().getWindow();
        stage.close();
    }
}
