//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SQL {

    private Connection c;
    private PreparedStatement stmt;
    private String code;
    private ResultSet rs;

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection a;

        Class.forName("org.sqlite.JDBC");
        a = DriverManager.getConnection("jdbc:sqlite:test.db");
        a.setAutoCommit(false);

        return a;
    }

    private void close() throws SQLException {
        rs.close();
        stmt.close();
        c.commit();
        c.close();
    }

    public void insertPost(float lat, float lon, String pcode, String city,
            String address, String office, String avail) throws SQLException, ClassNotFoundException {

        int geolocid, kaupunkiid, tunnisteid, postinumeroid;

        c = getConnection();

        //lisätään ensin vierasavainten tauluihin arvot
        //tarkastetaan, jos arvoja vastaava tietue on jo olemassa, ja jos on, niin käytetään sitä
        code = "SELECT geolocID FROM geoloc WHERE latitude = ? AND longitude = ?;";
        stmt = c.prepareStatement(code);
        stmt.setFloat(1, lat);
        stmt.setFloat(2, lon);
        rs = stmt.executeQuery();
        if (rs.isBeforeFirst()) {

            geolocid = rs.getInt(1);

        } else {
            //jos annettuille arvoille ei ole tietuetta, luodaan se
            code = "INSERT INTO geoloc (longitude, latitude) "
                    + "VALUES(?,?);";
            stmt = c.prepareStatement(code);
            stmt.setFloat(1, lon);
            stmt.setFloat(2, lat);
            stmt.executeUpdate();
            //ja luodun tietueen ID talteen
            code = "SELECT geolocid FROM geoloc ORDER BY geolocID DESC;";
            stmt = c.prepareStatement(code);
            rs = stmt.executeQuery();
            geolocid = rs.getInt(1);
        }

        //osoite + toimipaikka
        code = "SELECT tunnisteID FROM tunnisteet WHERE nimi = ?;";
        stmt = c.prepareStatement(code);
        stmt.setString(1, office);
        rs = stmt.executeQuery();
        if (rs.isBeforeFirst()) {

            tunnisteid = rs.getInt(1);

        } else {

            code = "INSERT INTO tunnisteet (katuosoite, nimi) VALUES (?,?);";
            stmt = c.prepareStatement(code);
            stmt.setString(1, address);
            stmt.setString(2, office);
            stmt.executeUpdate();

            code = "SELECT tunnisteID FROM tunnisteet ORDER BY tunnisteID DESC;";
            stmt = c.prepareStatement(code);
            rs = stmt.executeQuery();
            tunnisteid = rs.getInt(1);
        }

        //postinumero
        code = "SELECT postinumeroID FROM postinumerot WHERE postinumero = ?";
        stmt = c.prepareStatement(code);
        stmt.setString(1, pcode);
        rs = stmt.executeQuery();
        if (rs.isBeforeFirst()) {

            postinumeroid = rs.getInt(1);

        } else {

            code = "INSERT INTO postinumerot (postinumero) VALUES (?);";
            stmt = c.prepareStatement(code);
            stmt.setString(1, pcode);
            stmt.executeUpdate();

            code = "SELECT postinumeroID FROM postinumerot ORDER BY postinumeroID DESC;";
            stmt = c.prepareStatement(code);
            rs = stmt.executeQuery();
            postinumeroid = rs.getInt(1);
        }

        //kaupunki
        code = "SELECT kaupunkiID FROM kaupungit WHERE nimi = ?;";
        stmt = c.prepareStatement(code);
        stmt.setString(1, city);
        rs = stmt.executeQuery();
        if (rs.isBeforeFirst()) {

            kaupunkiid = rs.getInt(1);

        } else {

            code = "INSERT INTO kaupungit (nimi) VALUES (?);";
            stmt = c.prepareStatement(code);
            stmt.setString(1, city);
            stmt.executeUpdate();

            code = "SELECT kaupunkiID FROM kaupungit ORDER BY kaupunkiID DESC;";
            stmt = c.prepareStatement(code);
            rs = stmt.executeQuery();
            kaupunkiid = rs.getInt(1);
        }

        //tarkistetaan, löytyykö annetuilla arvoilla automaattia
        code = "SELECT * FROM automaatit WHERE geolocID = ? AND postinumeroID = ? "
                + "AND kaupunkiID = ? AND tunnisteID = ? AND aukioloajat = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, geolocid);
        stmt.setInt(2, postinumeroid);
        stmt.setInt(3, kaupunkiid);
        stmt.setInt(4, tunnisteid);
        stmt.setString(5, avail);
        rs = stmt.executeQuery();

        if (!rs.isBeforeFirst()) { //jos ei löydy, lisätään automaatti

            //lisätään automaatti tauluun
            code = "INSERT INTO automaatit (geolocID, postinumeroID, kaupunkiID, "
                    + "tunnisteID, aukioloajat) VALUES (?,?,?,?,?);";
            stmt = c.prepareStatement(code);
            stmt.setInt(1, geolocid);
            stmt.setInt(2, postinumeroid);
            stmt.setInt(3, kaupunkiid);
            stmt.setInt(4, tunnisteid);
            stmt.setString(5, avail);
            stmt.executeUpdate();
        }

        close();
    }

    //inserttaa esineen tauluun
    public void insertItem(Item a) throws ClassNotFoundException, SQLException {
        int mittaid;

        c = getConnection();

        //uuden esineen mitat tauluun
        code = "INSERT INTO mitat (leveys, syvyys, korkeus) VALUES (?,?,?);";
        stmt = c.prepareStatement(code);
        stmt.setFloat(1, a.getWidth());
        stmt.setFloat(2, a.getDepth());
        stmt.setFloat(3, a.getHeight());
        stmt.executeUpdate();

        //haetaan vierasavaimen arvo
        code = "SELECT mittaID FROM mitat ORDER BY mittaID DESC;";
        stmt = c.prepareStatement(code);
        rs = stmt.executeQuery();
        mittaid = rs.getInt(1);

        //esine tauluun
        code = "INSERT INTO esineet (nimi, hajoava, mittaid) VALUES (?,?,?);";
        stmt = c.prepareStatement(code);
        stmt.setString(1, a.getName());
        stmt.setBoolean(2, a.getBreakable());
        stmt.setInt(3, mittaid);
        stmt.executeUpdate();

        close();
    }

    //luo paketin tietokannan tietojen pohjalta, EI TEE TIETOKANTAAN MUUTOKSIA
    //parametrina paketin luokan numero
    public Package createPackage(int i) throws ClassNotFoundException, SQLException {
        int height, depth, width, dist;
        boolean breaks;
        Package a = null;

        c = getConnection();

        code = "SELECT leveys, korkeus, syvyys FROM mitat WHERE mittaID = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, i);
        rs = stmt.executeQuery();

        width = rs.getInt(1);
        height = rs.getInt(2);
        depth = rs.getInt(3);

        code = "SELECT etäisyys, hajottava FROM luokitukset WHERE luokitus = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, i);
        rs = stmt.executeQuery();

        dist = rs.getInt(1);
        breaks = rs.getBoolean(2);

        if (i == 1) {
            a = new FirstClass(dist, breaks, height, depth, width);
        } else if (i == 2) {
            a = new SecondClass(dist, breaks, height, depth, width);
        } else if (i == 3) {
            a = new ThirdClass(dist, breaks, height, depth, width);
        }

        close();

        return a;
    }

    //inserttaa paketin tauluun
    //parametreina paketti (sis.esineen) ja paketin luokitus
    public void insertPackage(Package a, int i) throws ClassNotFoundException, SQLException {
        int esineID;
        String itemName = a.getItem().getName();

        c = getConnection();

        //etsitään paketin esineen esineID vierasavainta varten
        code = "SELECT esineID FROM esineet WHERE nimi LIKE ?;";
        stmt = c.prepareStatement(code);
        stmt.setString(1, itemName);
        rs = stmt.executeQuery();

        esineID = rs.getInt(1);

        code = "INSERT INTO paketit (luokitus, esineID) VALUES (?, ?);";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, i);
        stmt.setInt(2, esineID);
        stmt.executeUpdate();

        close();
    }

    //tarkastaa, sopiiko esine pakettiin ja voiko paketti hajottaa esineen, EI TEE MUUTOKSIA TIETOKANTAAN
    //parametrit: paketin luokan numero, esine
    public boolean checkPackageFit(int i, Item a) throws ClassNotFoundException, SQLException {
        int pwidth, pheight, pdepth; //paketin specsit
        boolean breaks;

        c = getConnection();

        code = "SELECT leveys, korkeus, syvyys FROM mitat WHERE mittaID = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, i);
        rs = stmt.executeQuery();

        pwidth = rs.getInt(1);
        pheight = rs.getInt(2);
        pdepth = rs.getInt(3);

        code = "SELECT hajottava FROM luokitukset WHERE luokitus = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, i);
        rs = stmt.executeQuery();

        breaks = rs.getBoolean(1);

        close();

        if (a.getWidth() > pwidth || a.getHeight() > pheight
                || a.getDepth() > pdepth || (breaks && a.getBreakable())) {
            return false;
        } else {
            return true;
        }
    }

    //lataa SmartPost-automaatit tietokannasta ohjelmaan, kutsutaan aivan ohjelman alussa
    //EI TEE MUUTOKSIA TIETOKANTAAN
    public void loadPosts() throws ClassNotFoundException, SQLException {

        c = getConnection();

        code = "SELECT * FROM automaatitcheck;";
        stmt = c.prepareStatement(code);
        rs = stmt.executeQuery();

        while (rs.next()) {
            float lat = rs.getFloat(1);
            float lon = rs.getFloat(2);
            String postcode = rs.getString(3);
            String city = rs.getString(4);
            String address = rs.getString(5);
            String office = rs.getString(6);
            String avail = rs.getString(7);

            GeoLoc loc = new GeoLoc(lat, lon);
            SmartPost b = new SmartPost(postcode, city, address, office, avail, loc);

            Main.posts.add(b);
        }

        close();
    }

    //lataa esineet tietokannasta ohjelmaan, kutsutaan pääikkunaa avattaessa, EI TEE MUUTOKSIA TIETOKANTAAN
    public void loadItems() throws ClassNotFoundException, SQLException {

        c = getConnection();

        code = "SELECT * FROM esineetcheck;";
        stmt = c.prepareStatement(code);
        rs = stmt.executeQuery();

        while (rs.next()) {
            String name = rs.getString(1);
            boolean breakable = rs.getBoolean(2);
            int width = rs.getInt(3);
            int depth = rs.getInt(4);
            int height = rs.getInt(5);

            Item a = new Item(height, depth, width, name, breakable);
            Main.items.add(a);
        }

        close();
    }

    //tyhjentää tietokannasta varasto-taulun, kutsutaan aivan ohjelman alussa
    public void clearStorage() throws ClassNotFoundException, SQLException {

        c = getConnection();

        code = "DELETE FROM varasto";
        stmt = c.prepareStatement(code);
        stmt.executeUpdate();

        close();
    }

    //kaivaa varastocheck-viewistä varastotilanteen ja lisää paketit ohjelmaan, kutsutaan aivan alussa
    public void loadStorage() throws SQLException, ClassNotFoundException {

        c = getConnection();

        code = "SELECT * FROM varastocheck;";
        stmt = c.prepareStatement(code);
        rs = stmt.executeQuery();
        ArrayList<ArrayList> temp = new ArrayList();

        //varastocheckistä tiedot talteen tilapäislistaan
        while (rs.next()) {

            ArrayList temp2 = new ArrayList();

            temp2.add(rs.getString(1));
            temp2.add(rs.getInt(2));
            temp2.add(rs.getString(3));

            temp.add(temp2); //lista-ception
        }

        for (ArrayList b : temp) {//luodaan jokaista resultsetin riviä varten paketti
            Package a = createPackage((int) b.get(1));

            for (Item c : Main.items) { //helvetin listat
                if (c.getName().equals(b.get(2))) {
                    a.setItem(c);
                }
            }
            for (SmartPost d : Main.posts) { //miksei sqlite tue rinnakkaisia queryjä
                if (d.getName().equals(b.get(0))) {
                    a.setSource(d);
                }
            }

            Main.packages.add(a); //paketti ohjelmamuistiin
        }

        rs.close();
        c.close();

    }

    //kaivaa tilastocheck-viewistä tilaston ja palauttaa tiedot arraylistissä, käytetään tilastoikkunassa
    //EI MUOKKAA TIETOKANTAA
    public ArrayList<Tilastorivi> statsCheck() throws ClassNotFoundException, SQLException {

        ArrayList<Tilastorivi> statreturns = new ArrayList();

        c = getConnection();

        code = "SELECT * FROM tilastocheck ORDER BY ajankohta;";
        stmt = c.prepareStatement(code);
        rs = stmt.executeQuery();

        while (rs.next()) {
            String time = rs.getString(1);
            String source = rs.getString(2);
            String dest = rs.getString(3);
            int dist = rs.getInt(4);
            String name = rs.getString(5);

            Tilastorivi a = new Tilastorivi(time, source, dest, dist, name);
            statreturns.add(a); //luo uuden tilastorivi olion ja laittaa palautuslistaan
        }

        close();

        return statreturns; //sama juttu kuin varastocheckissä
    }

    //tilasto-tauluun lisääminen, paketin lähetyksen yhteydessä
    public void statsUpdate(Package a, double dist) throws ClassNotFoundException, SQLException {

        c = getConnection();
        SmartPost src = a.getSource();
        SmartPost dest = a.getDest();

        int srcid, destid, pakettiid;

        //lähtöautomaatin ID
        code = "SELECT automaattiID FROM automaatit aut "
                + "JOIN geoloc geo ON aut.geolocID = geo.geolocID "
                + "WHERE geo.latitude = ? AND geo.longitude = ?;";
        stmt = c.prepareStatement(code);
        stmt.setFloat(1, src.getLocation().getLat());
        stmt.setFloat(2, src.getLocation().getLon());
        rs = stmt.executeQuery();
        srcid = rs.getInt(1);

        //kohdeautomaatin ID
        code = "SELECT automaattiID FROM automaatit aut "
                + "JOIN geoloc geo ON aut.geolocID = geo.geolocID "
                + "WHERE geo.latitude = ? AND geo.longitude = ?;";
        stmt = c.prepareStatement(code);
        stmt.setFloat(1, dest.getLocation().getLat());
        stmt.setFloat(2, dest.getLocation().getLon());
        rs = stmt.executeQuery();
        destid = rs.getInt(1);

        //paketin ID
        code = "SELECT pakettiID FROM varasto "
                + "WHERE automaattiID = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, srcid);
        rs = stmt.executeQuery();
        pakettiid = rs.getInt(1);

        //tilastotauluun kirjoitus
        code = "INSERT INTO tapahtumatilasto (lähtöautomaattiID, kohdeautomaattiID, pakettiID, matka) "
                + "VALUES (?,?,?,?);";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, srcid);
        stmt.setInt(2, destid);
        stmt.setInt(3, pakettiid);
        stmt.setDouble(4, dist);
        stmt.executeUpdate();

        close();
    }

    //lisää paketin tietokannan varasto-tauluun, kutsutaan paketin luonnissa
    public void addPackageToStorage(Package a, SmartPost b) throws ClassNotFoundException, SQLException {

        c = getConnection();

        String automaattinimi = b.getName();
        String esinenimi = a.getItem().getName();
        int automaattiid, pakettiid;

        //etsitään annetun automaatin automaattiID
        code = "SELECT automaattiID FROM automaatit aut "
                + "JOIN tunnisteet tun ON aut.tunnisteID = tun.tunnisteID "
                + "WHERE tun.nimi LIKE ?";
        stmt = c.prepareStatement(code);
        stmt.setString(1, automaattinimi);
        rs = stmt.executeQuery();
        automaattiid = rs.getInt(1);

        //paketin pakettiID
        code = "SELECT pakettiID FROM paketit pak "
                + "JOIN esineet esi ON pak.esineID = esi.esineID "
                + "WHERE esi.nimi LIKE ? "
                + "ORDER BY pakettiID DESC;";
        stmt = c.prepareStatement(code);
        stmt.setString(1, esinenimi);
        rs = stmt.executeQuery();
        pakettiid = rs.getInt(1);

        //lisäys varastotauluun
        code = "INSERT INTO varasto (automaattiID, pakettiID) VALUES (?,?);";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, automaattiid);
        stmt.setInt(2, pakettiid);
        stmt.executeUpdate();

        close();
    }

    //siirtää paketin varastotaulussa automaatista toiseen, kutsutaan lähetyksen yhteydessä
    public void movePackageStorage(Package a) throws ClassNotFoundException, SQLException {

        c = getConnection();
        SmartPost src = a.getSource();
        SmartPost dest = a.getDest();

        int srcid, destid, pakettiid;

        //lähtöautomaatin ID
        code = "SELECT automaattiID FROM automaatit aut "
                + "JOIN geoloc geo ON aut.geolocID = geo.geolocID "
                + "WHERE geo.latitude = ? AND geo.longitude = ?;";
        stmt = c.prepareStatement(code);
        stmt.setFloat(1, src.getLocation().getLat());
        stmt.setFloat(2, src.getLocation().getLon());
        rs = stmt.executeQuery();
        srcid = rs.getInt(1);

        //kohdeautomaatin ID
        code = "SELECT automaattiID FROM automaatit aut "
                + "JOIN geoloc geo ON aut.geolocID = geo.geolocID "
                + "WHERE geo.latitude = ? AND geo.longitude = ?;";
        stmt = c.prepareStatement(code);
        stmt.setFloat(1, dest.getLocation().getLat());
        stmt.setFloat(2, dest.getLocation().getLon());
        rs = stmt.executeQuery();
        destid = rs.getInt(1);

        //paketin ID
        code = "SELECT pakettiID FROM varasto "
                + "WHERE automaattiID = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, srcid);
        rs = stmt.executeQuery();
        pakettiid = rs.getInt(1);

        //päivitä varastotaulu uudella automaattiIDllä
        code = "UPDATE varasto "
                + "SET automaattiID = ? "
                + "WHERE automaattiID = ?;";
        stmt = c.prepareStatement(code);
        stmt.setInt(1, destid);
        stmt.setInt(2, srcid);
        stmt.executeUpdate();

        close();
    }

    class Tilastorivi { //statscheck metodia varten luotu

        private final String time;
        private final String source;
        private final String dest;
        private final int dist;
        private final String name;

        public Tilastorivi(String a, String b, String c, int d, String e) {
            time = a;
            source = b;
            dest = c;
            dist = d;
            name = e;
        }

        public String getTime() {
            return time;
        }

        public String getSource() {
            return source;
        }

        public String getDest() {
            return dest;
        }

        public int getDist() {
            return dist;
        }

        public String getName() {
            return name;
        }
    }
}
