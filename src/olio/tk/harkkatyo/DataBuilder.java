//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

//Luokan tehtävä on käsitellä saatu XML-data
public class DataBuilder {

    private Document doc;
    private NodeList nodes;

    //alustaa XML-datan, (ernon video)
    public DataBuilder(String content) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println("EERRROROR");
        }
    }

    //parsii XML:stä automaattien tiedot, (ernon video)
    public void parsePostData() throws SQLException, ClassNotFoundException {
        nodes = doc.getElementsByTagName("place");
        String code, city, address, avail, office;
        float lat, lon;

        for (int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;

            //parsi elementistä tiedot
            code = getInfo("code", e);
            city = getInfo("city", e);
            address = getInfo("address", e);
            avail = getInfo("availability", e);
            office = getInfo("postoffice", e).trim();
            lat = Float.parseFloat(getInfo("lat", e));
            lon = Float.parseFloat(getInfo("lng", e));

            //uusi pakettiautomaatti on syntynyt
            Main.sql.insertPost(lat, lon, code, city, address, office, avail);
            

        }
    }

    //poimii XML-elementistä sisällön ja palauttaa sen
    public String getInfo(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
}
