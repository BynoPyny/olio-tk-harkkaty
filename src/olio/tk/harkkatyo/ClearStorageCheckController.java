//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import static olio.tk.harkkatyo.MainWindowController.populateCityChoices;
import static olio.tk.harkkatyo.MainWindowController.populatePackageChoices;
import static olio.tk.harkkatyo.OptionsMenuController.addError;

public class ClearStorageCheckController implements Initializable {

    @FXML
    private Button YesButton;
    @FXML
    private Button NoButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void NoAction(ActionEvent event) {

        try {
            Main.sql.loadStorage();
            addError("Varasto ladattu");
            
        } catch (SQLException | ClassNotFoundException ex) {
            addError("Varaston lataaminen epäonnistui");
        }

        populateCityChoices();
        populatePackageChoices();

        Stage stage = (Stage) NoButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void YesAction(ActionEvent event) {
        try {
            Main.sql.clearStorage();
            addError("Varasto tyhjennetty");
            
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
            addError("Varaston tyhjentäminen epäonnistui");
        }

        populateCityChoices();

        Stage stage = (Stage) YesButton.getScene().getWindow();
        stage.close();

    }
}
