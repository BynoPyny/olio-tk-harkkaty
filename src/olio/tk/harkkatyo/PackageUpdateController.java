//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

public class PackageUpdateController implements Initializable {

    @FXML
    private static ChoiceBox<String> PackageBox;
    @FXML
    private ChoiceBox<String> destBox;
    @FXML
    private Button acceptButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //täytä kohdeautomaattien lista
        for (SmartPost a : Main.posts) {
            destBox.getItems().add(a.getCity() + ": " + a.getName());
        }
    }

    @FXML
    private void acceptAction(ActionEvent event) {
        Package b = null; //paketti, jolle uusi kohde asetetaan

        if (destBox.getValue() == null || PackageBox.getValue() == null) {
            return;
        }

        String postinfos[] = destBox.getValue().split(": ");
        String packinfos[] = PackageBox.getValue().split(": ");

        //etsi valittu paketti
        for (Package c : Main.packages) {
            if (c.source.getCity().equals(packinfos[0])
                    && c.getItem().getName().equals(packinfos[1])) {
                b = c;
                break;
            }
        }

        //etsi valittu kaupunki/automaatti ja laita se paketin kohteeksi
        for (SmartPost a : Main.posts) {
            if (a.getCity().equals(postinfos[0])
                    && a.getName().equals(postinfos[1])) {
                b.setDest(a);
                break;
            }
        }

        Stage stage = (Stage) acceptButton.getScene().getWindow();
        stage.close();
    }

    //päivittää valikkoon paketit, joilla ei ole kohdetta, kutsutaan ikkunaa avatessa
    public static void updatePackageList() {
        PackageBox.getItems().clear();

        for (Package a : Main.packages) {
            if (a.getDest() == null) {
                PackageBox.getItems().add(a.source.getCity() + ": " + a.item.getName());
            }
        }
    }

}
