//Matias Salohonka
//0438604
//6.7.2016
package olio.tk.harkkatyo;


import java.sql.SQLException;
import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static olio.tk.harkkatyo.OptionsMenuController.addError;

public class Main extends Application {

    public static SQL sql;
    public static ArrayList<SmartPost> posts;
    public static ArrayList<Item> items;
    public static ArrayList<Package> packages;
    
    @Override
    public void start(Stage stage) throws Exception {
        
        posts = new ArrayList();
        items = new ArrayList();
        packages = new ArrayList();
        sql = new SQL();

        //luo pääikkuna
        Parent map = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        
        Scene scene = new Scene(map);
        stage.setScene(scene);
        stage.setTitle("SmartPost kartta");
        stage.initStyle(StageStyle.UTILITY);
        stage.show();

        //luo varastontyhjennys kyselyikkuna
        Stage stage2 = new Stage();
        Parent check = FXMLLoader.load(getClass().getResource("ClearStorageCheck.fxml"));
        
        Scene scene2 = new Scene(check);
        stage2.setScene(scene2);
        stage2.setTitle("Ladataanko varastotilanne");
        stage2.initStyle(StageStyle.UTILITY);
        stage2.show();
        
        //lataa automaatit ja esineet tietokannasta
        try {
            sql.loadPosts();

        } catch (ClassNotFoundException | SQLException ex) {
            addError("Pakettiautomaattien lataaminen tietokannasta epäonnistui");
        }

        try {
            sql.loadItems();

        } catch (ClassNotFoundException | SQLException ex) {
            addError("Tavaroiden lataaminen tietokannasta epäonnistui");
        }
    }

    public static void main(String[] args) {
        
        launch(args);
    }

}
