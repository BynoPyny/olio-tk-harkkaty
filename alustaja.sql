CREATE TABLE geoloc (
	geolocID		INTEGER PRIMARY KEY AUTOINCREMENT,
	latitude		REAL NOT NULL,
	longitude		REAL NOT NULL
);

CREATE TABLE postinumerot (
	postinumeroID	INTEGER PRIMARY KEY AUTOINCREMENT,
	postinumero		CHAR(6) NOT NULL UNIQUE
);

CREATE TABLE kaupungit (
	kaupunkiID		INTEGER PRIMARY KEY AUTOINCREMENT,
	nimi			VARCHAR(20) NOT NULL UNIQUE
);

CREATE TABLE tunnisteet (
	tunnisteID		INTEGER PRIMARY KEY AUTOINCREMENT,
	katuosoite		VARCHAR(30) NOT NULL,
	nimi			VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE automaatit (
	automaattiID	INTEGER PRIMARY KEY,
	geolocID		INTEGER NOT NULL,
	postinumeroID	INTEGER NOT NULL,
	kaupunkiID		INTEGER NOT NULL,
	tunnisteID		INTEGER NOT NULL,
	aukioloajat		VARCHAR(50) NOT NULL,

	FOREIGN KEY(geolocID) REFERENCES geoloc(geolocID),
	FOREIGN KEY(postinumeroID) REFERENCES postinumerot(postinumeroID),
	FOREIGN KEY(kaupunkiID) REFERENCES kaupungit(kaupunkiID),
	FOREIGN KEY(tunnisteID) REFERENCES tunnisteet(tunnisteID)
);

CREATE TABLE mitat (
	mittaID			INTEGER PRIMARY KEY,
	leveys			INTEGER DEFAULT 30,
	syvyys			INTEGER DEFAULT 30,
	korkeus			INTEGER DEFAULT 30
);

CREATE TABLE esineet (
	esineID			INTEGER PRIMARY KEY,
	nimi			VARCHAR(20) NOT NULL UNIQUE,
	hajoava			BOOLEAN DEFAULT TRUE,
	mittaID			INTEGER NOT NULL,

	FOREIGN KEY(mittaID) REFERENCES mitat(mittaID)
);

CREATE TABLE luokitukset (
	luokitus		INTEGER PRIMARY KEY AUTOINCREMENT,
	mittaID			INTEGER NOT NULL,
	hajottava		BOOLEAN NOT NULL,
	etäisyys		INTEGER NOT NULL,

	FOREIGN KEY(mittaID) REFERENCES mitat(mittaID)
);

CREATE TABLE paketit (
	pakettiID		INTEGER PRIMARY KEY AUTOINCREMENT,
	luokitus		INTEGER DEFAULT 3,
	esineID			INTEGER NOT NULL,

	FOREIGN KEY(luokitus) REFERENCES luokitukset(luokitus),
	FOREIGN KEY(esineID) REFERENCES esineet(esineID)
);

CREATE TABLE tapahtumatilasto (
	lähtöautomaattiID	INTEGER NOT NULL,
	kohdeautomaattiID	INTEGER NOT NULL,
	pakettiID			INTEGER NOT NULL,
	matka				REAL,
	ajankohta			DATETIME DEFAULT (DATETIME('now')),

	PRIMARY KEY(lähtöautomaattiID, kohdeautomaattiID, pakettiID),

	FOREIGN KEY(lähtöautomaattiID) REFERENCES automaatit(automaattiID),
	FOREIGN KEY(kohdeautomaattiID) REFERENCES automaatit(automaattiID),
	FOREIGN KEY(pakettiID) REFERENCES paketit(pakettiID)
);

CREATE TABLE varasto (
	pakettiID		INTEGER PRIMARY KEY,
	automaattiID	INTEGER NOT NULL,

	FOREIGN KEY(pakettiID) REFERENCES paketit(pakettiID),
	FOREIGN KEY(automaattiID) REFERENCES automaatit(automaattiID)
);

CREATE VIEW varastocheck AS
	SELECT tun.nimi AS automaatin_nimi, pak.luokitus AS paketin_luokitus, esi.nimi AS paketin_sisältö
		FROM varasto var
		JOIN automaatit aut ON var.automaattiID = aut.automaattiID
		JOIN tunnisteet tun ON aut.tunnisteID = tun.tunnisteID
		JOIN paketit pak ON var.pakettiID = pak.pakettiID
		JOIN esineet esi ON pak.esineID = esi.esineID
		ORDER BY var.automaattiID;

CREATE VIEW tilastocheck AS
	SELECT tap.ajankohta AS ajankohta, tu1.nimi AS lähtöpaikka, tu2.nimi AS kohdepaikka, tap.matka AS matka, esi.nimi AS tavara
		FROM tapahtumatilasto tap
		JOIN automaatit au1 ON tap.lähtöautomaattiID = au1.automaattiID
		JOIN tunnisteet tu1 ON au1.tunnisteID = tu1.tunnisteID
		JOIN automaatit au2 ON tap.kohdeautomaattiID = au2.automaattiID
		JOIN tunnisteet tu2 ON au2.tunnisteID = tu2.tunnisteID
		JOIN paketit pak ON tap.pakettiID = pak.pakettiID
		JOIN esineet esi ON pak.esineID = esi.esineID
		ORDER BY datetime(tap.ajankohta) DESC;

CREATE VIEW automaatitcheck AS
	SELECT geo.latitude, geo.longitude, pos.postinumero, kau.nimi, tun.katuosoite, tun.nimi, aut.aukioloajat
		FROM automaatit aut
		JOIN geoloc geo ON aut.geolocID = geo.geolocID
		JOIN postinumerot pos ON aut.postinumeroID = pos.postinumeroID
		JOIN kaupungit kau ON aut.kaupunkiID = kau.kaupunkiID
		JOIN tunnisteet tun ON aut.tunnisteID = tun.tunnisteID
		ORDER BY kau.nimi;

CREATE VIEW esineetcheck AS
	SELECT esi.nimi, esi.hajoava, mit.leveys, mit.syvyys, mit.korkeus
		FROM esineet esi
		JOIN mitat mit ON esi.mittaID = mit.mittaID;

INSERT INTO mitat (leveys, syvyys, korkeus)
	VALUES (50, 50, 30);

INSERT INTO mitat (leveys, syvyys, korkeus)
	VALUES (30, 30, 30);

INSERT INTO mitat (leveys, syvyys, korkeus)
	VALUES (80, 70, 70);

INSERT INTO mitat (leveys, syvyys, korkeus)
	VALUES (20, 15, 20);

INSERT INTO mitat (leveys, syvyys, korkeus)
	VALUES (40, 50, 20);

INSERT INTO mitat (leveys, syvyys, korkeus)
	VALUES (15, 20, 60);

INSERT INTO mitat (leveys, syvyys, korkeus)
	VALUES (60, 60, 60);

INSERT INTO luokitukset (mittaID, hajottava, etäisyys)
	VALUES (1, 1, 150);

INSERT INTO luokitukset (mittaID, hajottava, etäisyys)
	VALUES (2, 0, 9999);

INSERT INTO luokitukset (mittaID, hajottava, etäisyys)
	VALUES (3, 0, 9999);

INSERT INTO esineet (nimi, hajoava, mittaID)
	VALUES ('kukkaruukku', 1, 4);

INSERT INTO esineet (nimi, hajoava, mittaID)
	VALUES ('keilapallo', 0, 5);

INSERT INTO esineet (nimi, hajoava, mittaID)
	VALUES ('tolppa', 0, 6);

INSERT INTO esineet (nimi, hajoava, mittaID)
	VALUES ('IKEAn jakkara', 0, 7);
